import 'package:flutter/material.dart';
import 'package:my_app/MyPages/Login.dart';
import 'package:my_app/MyPages/edit_register.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text(
            'D A S H B O A R D ',
            style: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  gradient: const LinearGradient(colors: [
                    Color.fromARGB(255, 3, 69, 80),
                    Color.fromARGB(255, 54, 116, 173),
                  ])),
              child: ListView(scrollDirection: Axis.horizontal, children: [
                Row(
                  children: [
                    Center(
                      child: Padding(
                          //Gallery
                          padding: const EdgeInsets.all(20.0),
                          child: Container(
                              height: 200,
                              width: 300,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10)),
                              child: OutlinedButton.icon(
                                  icon: const Text(
                                    'GALLERY',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red),
                                  ),
                                  label: const Icon(
                                    Icons.photo,
                                    size: 40,
                                  ),
                                  onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              const Gallery()))))),
                    ),
                  ],
                ),
                Center(
                  child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                          height: 200,
                          width: 300,
                          decoration: BoxDecoration(
                              color: Colors.orange,
                              borderRadius: BorderRadius.circular(10)),
                          child: OutlinedButton.icon(
                              icon: const Text(
                                'HOME',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              label: const Icon(
                                Icons.app_registration,
                                size: 40,
                              ),
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          const Login()))))),
                ),
                //register
                Center(
                  child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                          height: 200,
                          width: 300,
                          decoration: BoxDecoration(
                              color: const Color.fromARGB(255, 48, 255, 7),
                              borderRadius: BorderRadius.circular(10)),
                          child: OutlinedButton.icon(
                              icon: const Text(
                                'EDIT',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                              label: const Icon(
                                Icons.edit_sharp,
                                size: 40,
                              ),
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          const EditProfil()))))),
                ),
                Center(
                  child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                          height: 200,
                          width: 300,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: const Color.fromARGB(255, 146, 71, 161)),
                          child: OutlinedButton.icon(
                              icon: const Text(
                                'LOG OUT',
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ),
                              label: const Icon(
                                Icons.power_off_outlined,
                                size: 40,
                                color: Color.fromARGB(255, 241, 238, 238),
                              ),
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          const Login()))))),
                ),
              ])),
        ));
  }
}

class Gallery extends StatelessWidget {
  const Gallery({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text(
          'G A L L E R Y',
          style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: Container(
        alignment: Alignment.center,

        //constraints: const BoxConstraints.expand(width: 400, height: 400),
        decoration: const BoxDecoration(
            image: DecorationImage(
          image: AssetImage('lib/images/viddiqposter.png'),
          fit: BoxFit.cover,
        )),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: const Color.fromARGB(255, 164, 190, 19),
                      boxShadow: const [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                          offset: Offset(6, 2),
                          blurRadius: 8.0,
                          spreadRadius: 7.0,
                        ),
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.1),
                          offset: Offset(-6, -2),
                          blurRadius: 6.0,
                          spreadRadius: 7.0,
                        ),
                      ]),
                  child: const Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Text(
                        'WE TRUST OURSELF ENOUGH TO ALWAYS DELIVER '
                        'A SERVICE BEYOND YOUR EXPECTATIONS.',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                ),
              ),
            ),
            //const SizedBox(height: 10),
            Expanded(
                child: ListView(
              scrollDirection: Axis.horizontal,
              addRepaintBoundaries: true,
              children: [
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        color: const Color.fromARGB(230, 0, 0, 0),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              'lib/images/kblogo2.png',
                              matchTextDirection: true,
                              height: 200,
                              width: 200,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color:
                                      const Color.fromARGB(255, 238, 230, 230),
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                alignment: Alignment.center,
                                width: 70,
                                height: 50,
                                child: const Text(
                                  'LOGO',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ],
                      ),
                    )),
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        color: const Color.fromARGB(230, 0, 0, 0),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Image.asset(
                              'lib/images/poster3.png',
                              height: 200,
                              width: 200,
                              matchTextDirection: true,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color: const Color.fromARGB(255, 57, 5, 66),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                alignment: Alignment.center,
                                width: 70,
                                height: 50,
                                child: const Text(
                                  'POSTER',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                )),
                          ),
                        ],
                      ),
                    )),
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        color: const Color.fromARGB(230, 0, 0, 0),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.asset(
                              'lib/images/Mtnappremake.png',
                              matchTextDirection: true,
                              height: 200,
                              width: 200,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.yellowAccent,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                alignment: Alignment.center,
                                width: 70,
                                height: 50,
                                child: const Text(
                                  'LOGO',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ],
                      ),
                    )),
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        color: const Color.fromARGB(230, 0, 0, 0),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(400),
                            child: Image.asset(
                              'lib/images/viddiqposter.png',
                              matchTextDirection: true,
                              height: 200,
                              width: 200,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.blue.shade900,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                alignment: Alignment.center,
                                width: 70,
                                height: 50,
                                child: const Text(
                                  'POSTER',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ],
                      ),
                    )),
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        color: const Color.fromARGB(230, 0, 0, 0),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(400),
                            child: Image.asset(
                              'lib/images/PartyPoster1KB.png',
                              matchTextDirection: true,
                              height: 200,
                              width: 200,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.lime,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                alignment: Alignment.center,
                                width: 110,
                                height: 50,
                                child: const Text(
                                  'EVENT POSTER',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ],
                      ),
                    )),
                Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      padding: const EdgeInsets.all(15.0),
                      width: 250,
                      height: 250,
                      decoration: BoxDecoration(
                        color: const Color.fromARGB(230, 0, 0, 0),
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(400),
                            child: Image.asset(
                              'lib/images/kbBC.png',
                              matchTextDirection: true,
                              height: 200,
                              width: 200,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                          const SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.blueAccent,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                                alignment: Alignment.center,
                                width: 130,
                                height: 50,
                                child: const Text(
                                  'BUSINESS CARD',
                                  style: TextStyle(fontWeight: FontWeight.w700),
                                )),
                          ),
                        ],
                      ),
                    )),
              ],
            )),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                      height: 40,
                      width: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.black,
                      ),
                      child: OutlinedButton.icon(
                          icon: const Text(
                            'DASHBOARD',
                            style: TextStyle(color: Colors.white),
                          ),
                          label: const Icon(Icons.dashboard_customize_rounded),
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      const Dashboard())))),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                      height: 40,
                      width: 150,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Colors.black,
                      ),
                      child: OutlinedButton.icon(
                          icon: const Text(
                            'HOME',
                            style: TextStyle(color: Colors.white),
                          ),
                          label: const Icon(Icons.home_max_rounded),
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      const Login())))),
                ),
              ],
            ),

            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
